<?php

/**
 * @file
 * NSW theme code file.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Implements hook_preprocess_block__BLOCK_ID().
 */
function agov_nsw_preprocess_block__search_form_block(&$vars) {

  // We have the searchbox twice, one light, one dark.
  $modifier = $vars['elements']['#id'] == 'agov_nsw_searchform_2' ? '--light' : '';

  // The search form isn't all in one template, this code puts classes in about
  // three different templates.
  $vars['content']['#attributes']['class'][] = 'search search-form search' . $modifier;
  $vars['content']['keys']['#wrapper_attributes']['class'][] = 'search__input-wrapper search__inner-wrapper' . $modifier;
  $vars['content']['keys']['#attributes']['class'][] = 'search__input search__input' . $modifier;
  $vars['content']['keys']['#attributes']['placeholder'] = t('Enter your keywords');

  // Customise the submit button.
  $vars['content']['actions']['submit']['#title_display'] = 'invisible';
  $vars['content']['actions']['submit']['#attributes']['class'][] = 'search__button' . $modifier;
  $vars['content']['actions']['submit']['#attributes']['class'][] = 'search__button';
  // Add the search icon.
  $vars['content']['actions']['submit']['#prefix'] = '<div class="search__button-wrapper' . $modifier . '"><i class="fa fa-search search__icon" aria-hidden="true"></i>';
  $vars['content']['actions']['submit']['#suffix'] = '</div>';
}

/**
 * Implements hook_preprocess_block_agov_nsw_mainnavigation().
 */
function agov_nsw_preprocess_block__agov_nsw_mainnavigation(&$vars) {
  $vars['attributes']['class'][] = 'primary-navigation';
  $vars['title_attributes']['class'][] = 'primary-navigation__title';
  $vars['content']['#attributes']['placement_id'] = $vars['elements']['#id'];
}

/**
 * Implements hook_preprocess_block_agov_nsw_quicklinks_2_menu().
 */
function agov_nsw_preprocess_block__agov_nsw_quicklinks_2_menu(&$vars) {
  $vars['attributes']['class'][] = 'three-column-menu__list';
}

/**
 * Implements hook_preprocess_menu__agov_nsw_mainnavigation().
 */
function agov_nsw_preprocess_menu__agov_nsw_mainnavigation(&$vars) {
  $vars['attributes']['class'][] = 'primary-navigation__list';
  $vars['attributes']['class'][] = 'layout-center';

  foreach ($vars['items'] as $item) {
    $item['attributes']->addClass('primary-navigation__list-item');
    if (isset($item['below'])) {
      foreach ($item['below'] as $sub_item) {
        $sub_item['attributes']->addClass('primary-navigation__list-item-level-2');
      }
    }
  }
  $vars['sub_menu_attributes'] = new Attribute(['class' => 'primary-navigation__list-level-2']);
}

/**
 * Implements hook_preprocess_block__agov_nsw_mainnavigation_2().
 */
function agov_nsw_preprocess_block__agov_nsw_mainnavigation_2(&$vars) {
  $vars['attributes']['class'][] = 'divider';
  $vars['content']['#attributes']['class'] = ['unstyled-list sidebar-menu'];
}

/**
 * Implements hook_preprocess_block__mainnavigation_3().
 */
function agov_nsw_preprocess_block__agov_nsw_mainnavigation_3(&$vars) {
  $vars['attributes']['class'] += [
    'three-column-menu',
    'footer-top__menu'
  ];
  $vars['content']['#attributes']['placement_id'] = $vars['elements']['#id'];
}

/**
 * Implements hook_preprocess_menu__agov_nsw_mainnavigation_3().
 */
function agov_nsw_preprocess_menu__agov_nsw_mainnavigation_3(&$vars) {
  $vars['attributes']['class'][] = 'three-column-menu__list';
  foreach ($vars['items'] as $item) {
    $item['attributes']->addClass('three-column-menu__item');
  }
}

/**
 * Implements hook_preprocess_block__agov_nsw_footerquicklinks().
 */
function agov_nsw_preprocess_block__agov_nsw_footerquicklinks(&$vars) {
  $vars['attributes']['class'] += [
    'three-column-menu',
    'footer-top__menu',
  ];
  $vars['content']['#attributes']['placement_id'] = $vars['elements']['#id'];
}

/**
 * Implements hook_preprocess_menu__agov_nsw_footerquicklinks().
 */
function agov_nsw_preprocess_menu__agov_nsw_footerquicklinks(&$vars) {
  $vars['attributes']['class'][] = 'three-column-menu__list';
}
